import React from 'react';

const logo = require('./logo.svg') as string;
const Spinner=()=>{
    return(
        <div className="Spinner">
        <img src={logo} className="Spinner-logo" alt="logo" />
      </div>
    );
}
export default Spinner;
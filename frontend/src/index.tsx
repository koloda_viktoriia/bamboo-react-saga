import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import configureStore from './redux/store';
import { Provider } from 'react-redux';
import App from './App';
import { BrowserRouter as Router, Route } from 'react-router-dom';

const store = configureStore();
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store} >
      <Router >
        <Route path="/" component={App} />
      </Router>
    </Provider >
  </React.StrictMode>,
  document.getElementById('root')
);



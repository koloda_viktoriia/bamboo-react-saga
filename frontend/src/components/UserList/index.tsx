import React, { useEffect, useState } from 'react';
import './UserList.css';
import { Redirect, useHistory } from 'react-router-dom';
import * as types from '../../types';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import * as actions from '../../redux/actions';
import Spinner from '../../Spinner';

const UserList = () => {
  const userProfile = useSelector((state: types.ChatState) => state.userProfile, shallowEqual);
  const users = useSelector((state: types.ChatState) => state.users, shallowEqual);
  const isLogin = useSelector((state: types.ChatState) => state.isLogin, shallowEqual);
  const [isLoading, setisLoading] = useState(true);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(actions.fetchUsers());
    setisLoading(false);
  }, []);
  if (!isLogin) {
    return <Redirect to="/login" />;
  }
  if (userProfile.role !== 'Admin') {
    return <Redirect to="/chat" />;
  }

  const handleAddUser = (event: any) => {
    event.preventDefault();
    history.push('/user');
  }
  const handleToChat = (event: any) => {
    event.preventDefault();
    history.push('/chat');
  }
  const handleDeleteUser = (id: string) => {
    dispatch(actions.deleteUser(id));
  }
  const handleEditUser = (event: any, user: types.User) => {
    event.preventDefault();
    history.push(`/user/${user.userId}`);
  }

  return (isLoading ?
    <Spinner />
    :
    <div className="User-list">
      <button onClick={(ev) => handleAddUser(ev)}>
        Add user
        </button>
      <button onClick={(ev) => handleToChat(ev)}>
        Chat
        </button>
      <div className="User-grid">
        <div className="User-header">
          <div>Avatar</div>
          <div>User</div>
          <div>Password</div>
          <div>Role</div>
        </div>
        {users?.map(user => {
          return (
            <div className="User">
              <img src={user.avatar} className="User-Item img" />
              <div className="User-Item">{user.user}</div>
              <div className="User-Item">{user.password}</div>
              <div className="User-Item">{user.role}</div>
              <button onClick={(ev) => handleEditUser(ev, user)} >Edit</button>
              <button onClick={(ev) => handleDeleteUser(user.userId)}> Delete</button>
            </div>
          );
        })}
      </div>
    </div>
  );

}

export default UserList;

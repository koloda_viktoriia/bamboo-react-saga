import React, { useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import Header from '../Header/Header';
import * as actions from '../../redux/actions';
import { useHistory, Redirect } from 'react-router-dom';
import * as types from '../../types';
import './LoginPage.css';

const LoginPage = () => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const isLogin = useSelector((state: types.ChatState) => state.isLogin, shallowEqual);
    const user = useSelector((state: types.ChatState) => state.userProfile, shallowEqual);
    const history = useHistory();
    const dispatch = useDispatch();
    if (isLogin) {
        return <Redirect to="/" />;
    }
    
    const handleLoginClick = (event: any) => {
        event.preventDefault();
        dispatch(actions.login(login, password));
        history.push('/');

    };

    return (
        <>
        <Header/>
        <div className="Login-page">
         <form onSubmit={(ev) => handleLoginClick(ev)} className="Login-form">
            <label>Login:</label>
            <input
                value={login}
                onChange={(ev) => setLogin(ev.target.value)}
                className="login" />
            <label>Password:</label>
            <input
                value={password}
                onChange={(ev) => setPassword(ev.target.value)}
                className="password" />
            <button type="submit" >
                Sign in
          </button>
        </form>
        </div>
        </>
    );
}
export default LoginPage;
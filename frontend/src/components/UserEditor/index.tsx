import React, { useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Route, Redirect, useHistory, useParams } from 'react-router-dom';
import './UserEditor.css';
import * as actions from '../../redux/actions';
import * as types from '../../types';

const UserEditor = () => {
    const { userId } = useParams();
    const users = useSelector((state: types.ChatState) => state.users, shallowEqual);
    let editUser ;
    if (userId) {
      editUser = users.find(user => user.userId === userId);
   }
    const [avatar, setAvatar] = useState(editUser?.avatar as string);
    const [user, setUser] = useState(editUser?.user as string);
    const [password, setPassword] = useState(editUser?.password as string);
    const [role, setRole] = useState(editUser? editUser.role :'User');
    const isLogin = useSelector((state: types.ChatState) => state.isLogin, shallowEqual);
    const dispatch = useDispatch();
    const history = useHistory();
    console.log('userid');
    console.log(userId);
    if (!isLogin) {
        return <Redirect to="/" />;
    }


       const handleOnAddClick = async (event: any) => {
        const newUser = {
            userId: '',
            avatar,
            password,
            user,
            role
        }
        dispatch(actions.addUser(newUser));
        history.push("/");

    };
    const handleOnCancelClick = () => {
        history.push("/");
    }
    const handleOnEditClick = () => {
        const editUser = {
            userId,
            avatar,
            password,
            user,
            role
        }
        dispatch(actions.editUser(editUser));
        history.push("/");
    }

    return (
        <>
            <div className="Edit-page">
                <div className="Edit-form">
                    <label>Avatar:</label>
                    <input
                        value={avatar}
                        onChange={(ev) => setAvatar(ev.target.value)}
                        className="avatar" />
                    <label>User:</label>
                    <input
                        value={user}
                        onChange={(ev) => setUser(ev.target.value)}
                        className="user" />
                    <label>Password:</label>
                    <input
                        value={password}
                        onChange={(ev) => setPassword(ev.target.value)}
                        className="password" />
                    <label>Role:</label>
                    <input
                        value={role}
                        onChange={(ev) => setRole(ev.target.value)}
                        className="role" />
                        <div className="Edit-buttons">
                    <button type="submit" onClick={handleOnCancelClick} >Cancel</button>
                    {userId !== undefined ?
                        <button type="submit" onClick={handleOnEditClick} >Edit</button> :
                        <button type="submit" onClick={handleOnAddClick} >Add</button>
                        
                    }
                    </div>
                </div>
            </div>
        </>
    );

}
export default UserEditor;
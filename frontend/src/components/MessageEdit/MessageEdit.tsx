import React, { useState } from 'react';
import './MessageEdit.css';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {useParams, useHistory} from 'react-router-dom';
import * as actions from '../../redux/actions';
import * as types from '../../types';

const MessageEdit = () => {
  const { id } = useParams();
  const messages = useSelector((state: types.ChatState) => state.messages, shallowEqual);
  let  editMessage = messages.find(message => message.id === id);
  const [textValue, setTextValue] = useState(editMessage?.text);
  const dispatch = useDispatch();
  const history = useHistory();

  const onCancel = () => {
    history.push("/chat");
  }

  const onEdit = () => {
    if(textValue!==undefined && id!==undefined){
    dispatch(actions.editMessage(textValue, id));
    history.push("/chat");
    }
  }

  return (

    <>
      <div className="Modal-background">
        <div id="Modal">
          <div className="Modal-header">
            <span>Edit message</span>
          </div>
          <div className="Modal-edit">
            <textarea
              onChange={(ev) => setTextValue(ev.target.value)}
              value={textValue}/>
            <div className="Modal-buttons">
              <button onClick={onCancel} className="cancelButton">Cancel</button>
              <button onClick={onEdit} className="editButton">Edit</button>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}



export default MessageEdit;

import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Redirect, useHistory } from 'react-router-dom';
import Header from '../Header/Header';
import Spinner from '../../Spinner';
import ChatHeader from '../ChatHeader/ChatHeader';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import Footer from '../Footer/Footer';
import './Chat.css';
import * as actions from '../../redux/actions';
import * as types from '../../types';


const Chat = () => {
  const messages: types.Message[] = useSelector((state: types.ChatState) => state.messages, shallowEqual);
  const likes: types.Like[] | undefined = useSelector((state: types.ChatState) => state.likes, shallowEqual);
  const userProfile: types.User = useSelector((state: types.ChatState) => state.userProfile, shallowEqual);
  const isLogin: Boolean = useSelector((state: types.ChatState) => state.isLogin , shallowEqual);
  const history = useHistory();
  const dispatch = useDispatch();
  const [isLoading, setisLoading] = useState(true);

  useEffect(() => {
    dispatch(actions.fetchMessages());
    dispatch(actions.fetchLikes());
    setisLoading(false);
  },[]);

  console.log(isLogin);
  if (!isLogin)  {
    return <Redirect to="/login" />;
  }

  messages?.sort(
    (a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    });

  const showEdit = (id?: string) => {
    if (id === undefined) {
      const userMessages = messages
        .filter(message => message.userId === userProfile.userId);
      if (userMessages.length === 0) return;
      const message = userMessages.reduce((prev, current) => (prev.createdAt > current.createdAt) ? prev : current);
       id = message.id;
    }
    history.push(`/edit/${id}`);
  }

  const addMessage = (text: string) => {
    dispatch(actions.addMessage(text));
  }

  const deleteMessage = (id: string) => {
    dispatch(actions.deleteMessage(id));
  }

  const liked = (userId: string, id: string, isLike: Boolean) => {
    dispatch(actions.liked(userId, id, isLike));
  }

  return (isLoading
    ?
  <Spinner/>
    :
    <>
      <Header />
      <div className="Chat">
        <ChatHeader data={messages} />
        <MessageList data={messages}
          deleteMessage={deleteMessage}
          likes={likes}
          showEdit={showEdit}
          userId={userProfile?.userId}
          liked={liked} />
        <MessageInput addMessage={addMessage} showEdit={showEdit} />
      </div>
      <Footer />

    </>
  );

}

export default Chat;

import React from 'react';
import './Header.css';

import logo from './logo.svg';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import * as types from '../../types';
import * as actions from '../../redux/actions';
import { useHistory } from 'react-router';
const Header = () => {
  const isLogin = useSelector((state: types.ChatState) => state.isLogin, shallowEqual);
  const {role}  = useSelector ((state: types.ChatState)=> state.userProfile , shallowEqual);
  const dispatch = useDispatch();
  const history = useHistory();
  const handleLogOut = () => {
     dispatch(actions.logOut());
 }
 const handleToUsers = () => {
  history.push("/");
}
  return (
    <header>
      <img src={logo} className="Header-logo" alt="logo" />
      <span>Bamboo</span>
      <div  className="buttons">
     { isLogin && <button onClick={() => handleLogOut()} > Log Out</button>}
     { role=== "Admin" && <button onClick={() => handleToUsers()} >Users</button>}
     </div>
    </header>
  );
}

export default Header;
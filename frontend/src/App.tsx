import React, { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import Chat from './components/Chat/Chat';
import UserList from './components/UserList';
import UserEditor from './components/UserEditor';
import MessageEdit from './components/MessageEdit/MessageEdit';
import LoginPage from './components/LoginPage/LoginPage';
const App = () => {
   return (
<div className="App">
			<Switch>
		  <Route exact path='/login' component={LoginPage} />
      <Route exact path='/' component={UserList} />
      <Route exact path='/user' component={UserEditor} />
      <Route exact path="/chat" component={Chat} />
      <Route exact path="/edit/:id" component={MessageEdit} />
      <Route path='/user/:userId' component={UserEditor} />
			</Switch>
		</div>
  
  );

}

export default App;

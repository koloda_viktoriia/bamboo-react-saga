import { FETCH_MESSAGES } from "./actionTypes";
import { User } from "../types";

export const addMessage = (text: string) => {
    return { type: 'ADD_MESSAGE', payload: { text } };
}
export const editMessage = (text: string, id: string) => {
    return { type: 'UPDATE_MESSAGE', payload: { text, id } };

}
export const deleteMessage = (id: string) => {
    return { type: 'DELETE_MESSAGE', payload: { id } };

}
export const liked = (userId: string, id: string, isLike: Boolean) => {
    return { type: 'LIKED', payload: { userId, id, isLike } };
}

export const fetchMessages = () => ({
    type: 'FETCH_MESSAGES'
});
export const fetchLikes = () => ({
    type: 'FETCH_LIKES'
});
export const login = (user: String, password: String) => ({
    type: 'LOGIN',
    payload: { user, password }
});
export const logOut = () => ({
    type: 'LOG_OUT'
});

export const addUser = (user: User) => {
    return { type: 'ADD_USER', payload: { user } };
}
export const editUser = (user: User) => {
    return { type: 'UPDATE_USER', payload: { user } };

}
export const deleteUser = (id: string) => {
    return { type: 'DELETE_USER', payload: { id } };

}
export const fetchUsers = () => ({
    type: 'FETCH_USERS'
});
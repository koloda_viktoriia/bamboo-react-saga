import { ChatState, Message, Like, editMSG, User } from '../types';
import { ChatActionTypes } from './actionTypes';

const user = localStorage.getItem("userProfile");
const auth = localStorage.getItem("isLogin");

const INITIAL_STATE: ChatState = {
    messages: [],
    likes: [] as Like[],
    users: [] as User[],
    userProfile: user === null ? {} as User : JSON.parse(user),
    isLogin: auth !== null
};

export const chatReducer = (state = INITIAL_STATE, action: ChatActionTypes): ChatState => {
    switch (action.type) {
        case 'FETCH_MESSAGES_SUCCESS':
            const newMessages: Message[] = action.payload.messages;
            return { ...state, messages: newMessages };
        case 'FETCH_LIKES_SUCCESS':
            const newLikes: Like[] = action.payload.likes;
            return { ...state, likes: newLikes };
        case 'LOGIN_SUCCESS':
            const user = action.payload.user;
            localStorage.setItem("userProfile", JSON.stringify(user));
            localStorage.setItem("isLogin", JSON.stringify(true));
            return { ...state, userProfile: user, isLogin: true }
        case 'LOG_OUT':
            localStorage.removeItem("userProfile");
            localStorage.removeItem("IsLogin");
            return { ...state, userProfile: {} as User, isLogin: false }
        case 'FETCH_USERS_SUCCESS':
            const users = action.payload.users;
            return { ...state, users: users };
        default:
            return state;
    }
}


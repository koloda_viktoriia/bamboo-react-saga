import * as types from '../types';
import LoginPage from '../components/LoginPage/LoginPage';

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const UPDATE_MESSAGE = 'UPDATE_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const LIKED = 'LIKED';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES = 'FETCH_MESSAGES';
export const FETCH_LIKES_SUCCESS = 'FETCH_LIKES_SUCCESS';
export const FETCH_LIKES = 'FETCH_LIKES';
export const LOGIN = 'LOGIN';
export const LOG_OUT = 'LOG_OUT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS = 'FETCH_USERS';

export interface AddMessage {
    type: typeof ADD_MESSAGE;
    payload: { text: string };
}
export interface EditMessage {
    type: typeof UPDATE_MESSAGE;
    payload: { text: string, id: string }
}
export interface DeleteMessage {
    type: typeof DELETE_MESSAGE;
    payload: { id: string };
}

export interface Liked {
    type: typeof LIKED;
    payload: { userId: string, id: string, isLike: Boolean };
}

export interface FetchMessages {
    type: typeof FETCH_MESSAGES_SUCCESS
    payload: { messages: types.Message[] }
}
export interface FetchLikes {
    type: typeof FETCH_LIKES_SUCCESS
    payload: { likes: types.Like[] }
}
export interface LoginSuccess {
    type: typeof LOGIN_SUCCESS
    payload: { user: types.User }
}
export interface Login {
    type: typeof LOGIN
    payload: { user: String, password: String }
}
export interface LogOut {
    type: typeof LOG_OUT
}
export interface AddUser {
    type: typeof ADD_MESSAGE;
    payload: { user: types.User };
}
export interface EditUser {
    type: typeof UPDATE_MESSAGE;
    payload: { user: types.User }
}
export interface DeleteUser {
    type: typeof DELETE_MESSAGE;
    payload: { id: string };
}
export interface FetchUsers {
    type: typeof FETCH_USERS_SUCCESS
    payload: { users: types.User[] }
}

export type ChatActionTypes = FetchMessages | FetchLikes | LoginSuccess | LogOut | FetchUsers 
import { chatReducer } from './reducer';
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas/sagas';

export default function configureStore() {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        chatReducer,
        applyMiddleware(sagaMiddleware)
    );

    sagaMiddleware.run(rootSaga);

    return store;
}

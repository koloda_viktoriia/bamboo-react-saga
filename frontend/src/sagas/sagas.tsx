import { all } from 'redux-saga/effects';
import messageSagas from './messageSagas';
import likeSagas  from './likeSagas';
import userSagas from './userSagas';
import loginSagas from './loginSagas';

export default function* rootSaga() {
    yield all([
        likeSagas(),
        messageSagas(),
        userSagas(),
        loginSagas()
    ])
};
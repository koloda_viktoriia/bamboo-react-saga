import axios from 'axios';
import api from '../config/api.json';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as actionTypes from "../redux/actionTypes";

let axiosConfig = {
    headers: {
        'Content-Type': 'application/json'
    }
};


export function* fetchLikes() {
    console.log('fethch likes');
    try {
        const answer = yield call(axios.get, `${api.url}/likes`);
        yield put({ type: 'FETCH_LIKES_SUCCESS', payload: { likes: answer.data } })
    } catch (error) {
        console.log('fetchLikes error:', error)
    }
}

function* watchFetchLikes() {
    yield takeEvery(actionTypes.FETCH_LIKES, fetchLikes)
}

export function* liked(action: actionTypes.Liked) {
    const {isLike, id, userId} = action.payload;
    const like = {
      id,
      userId
    }
    console.log(like);
    try {
        if(isLike){
            yield call(axios.put, `${api.url}/likes`, like, axiosConfig);
        } else{
        yield call(axios.post, `${api.url}/likes`, like, axiosConfig);
        }
        yield put({ type: actionTypes.FETCH_LIKES });
    } catch (error) {
        console.log('createUser error:', error.message);
    }
}

function* watchLiked() {
    yield takeEvery(actionTypes.LIKED, liked)
}



export default function* likeSagas() {
    yield all([
        watchFetchLikes(),
        watchLiked()
    ])
};

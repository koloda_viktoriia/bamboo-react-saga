import axios from 'axios';
import api from '../config/api.json';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as actionTypes from "../redux/actionTypes";
import * as types from '../types';
import userProfile from '../user.json';

import { v4 as uuidv4 } from 'uuid';

let axiosConfig = {
    headers: {
        'Content-Type': 'application/json'
    }
};


export function* fetchMessages() {
    try {
        const answer = yield call(axios.get, `${api.url}/messages`);
        yield put({ type: 'FETCH_MESSAGES_SUCCESS', payload: { messages: answer.data } })
    } catch (error) {
        console.log('fetchMESSAGES error:', error)
    }
}

function* watchFetchMessages() {
    yield takeEvery(actionTypes.FETCH_MESSAGES, fetchMessages)
}

export function* addMessage(action: actionTypes.AddMessage) {
    const { text } = action.payload;
    const message = {
        id: uuidv4(),
        userId: userProfile.userId,
        avatar: userProfile.avatar,
        user: userProfile.user,
        text: text,
        createdAt: new Date().getTime(),
        editedAt: ""
    }
    try {
        yield call(axios.post, `${api.url}/messages`, message, axiosConfig);
        yield put({ type: actionTypes.FETCH_MESSAGES });
    } catch (error) {
        console.log('createUser error:', error.message);
    }
}

function* watchAddMessage() {
    yield takeEvery(actionTypes.ADD_MESSAGE, addMessage)
}

export function* updateMessage(action: actionTypes.EditMessage) {
    const message = action.payload;
    console.log('mes');
    console.log(message);
    try {
        yield call(axios.put, `${api.url}/messages`, message,axiosConfig);
        yield put({ type: actionTypes.FETCH_MESSAGES });
    } catch (error) {
        console.log('updateMessage error:', error.message);
    }
}

function* watchUpdateMessage() {
    yield takeEvery(actionTypes.UPDATE_MESSAGE, updateMessage)
}

export function* deleteMessage(action: actionTypes.DeleteMessage) {
    try {
        yield call(axios.delete, `${api.url}/messages/${action.payload.id}`);
        yield put({ type: actionTypes.FETCH_MESSAGES })
    } catch (error) {
        console.log('deleteUser Error:', error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(actionTypes.DELETE_MESSAGE, deleteMessage)
}

export default function* messageSagas() {
    yield all([
        watchFetchMessages(),
        watchAddMessage(),
        watchUpdateMessage(),
        watchDeleteMessage()
    ])
};

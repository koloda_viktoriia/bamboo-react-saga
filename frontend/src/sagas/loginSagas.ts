import axios from 'axios';
import api from '../config/api.json';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as actionTypes from "../redux/actionTypes";

let axiosConfig = {
    headers: {
        'Content-Type': 'application/json'
    }
};


export function* login(action: actionTypes.Login) {
    console.log('login');
    try {
        console.log(action.payload);
        const answer = yield call(axios.post, `${api.url}/login`, action.payload,axiosConfig);
        console.log(answer.data);
        if(answer.data!==null){
        yield put({ type: 'LOGIN_SUCCESS', payload: {user: answer.data} } );
        }
    } catch (error) {
        console.log('fetch User error:', error);
    }
}


function* watchLogin() {
    yield takeEvery(actionTypes.LOGIN, login)
}



export default function* loginSagas() {
    yield all([
        watchLogin()
    ])
};

import axios from 'axios';
import api from '../config/api.json';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import * as actionTypes from "../redux/actionTypes";
import * as types from '../types';
import userProfile from '../user.json';

import { v4 as uuidv4 } from 'uuid';

let axiosConfig = {
    headers: {
        'Content-Type': 'application/json'
    }
};


export function* fetchUsers() {
    try {
        const answer = yield call(axios.get, `${api.url}/users`);
        console.log(answer);
        yield put({ type: 'FETCH_USERS_SUCCESS', payload: { users: answer.data } })
    } catch (error) {
        console.log('fetchUsers error:', error)
    }
}

function* watchFetchUsers() {
    yield takeEvery(actionTypes.FETCH_USERS, fetchUsers)
}

export function* addUser(action: actionTypes.AddUser) {
    let user = action.payload.user;
     user.userId = uuidv4();
    try {
        yield call(axios.post, `${api.url}/users`,user, axiosConfig);
        yield put({ type: actionTypes.FETCH_USERS });
    } catch (error) {
        console.log('createUser error:', error.message);
    }
}

function* watchAddUser() {
    yield takeEvery(actionTypes.ADD_USER, addUser)
}

export function* updateUser(action: actionTypes.EditUser) {
    const user = action.payload;
    try {
        yield call(axios.put, `${api.url}/users`, user ,axiosConfig);
        yield put({ type: actionTypes.FETCH_USERS });
    } catch (error) {
        console.log('updateUser error:', error.message);
    }
}

function* watchUpdateUser() {
    yield takeEvery(actionTypes.UPDATE_USER, updateUser)
}

export function* deleteUser(action: actionTypes.DeleteUser) {
    try {
        yield call(axios.delete, `${api.url}/users/${action.payload.id}`);
        yield put({ type: actionTypes.FETCH_USERS})
    } catch (error) {
        console.log('deleteUser Error:', error.message);
    }
}

function* watchDeleteUser() {
    yield takeEvery(actionTypes.DELETE_USER, deleteUser)
}

export default function* messageUser() {
    yield all([
        watchFetchUsers(),
        watchAddUser(),
        watchUpdateUser(),
        watchDeleteUser()
    ])
};

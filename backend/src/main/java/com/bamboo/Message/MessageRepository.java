package com.bamboo.Message;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class MessageRepository {
    private List<MessageDto> messagesCache;
    MessageRepository(){
        Gson gson = new Gson();
        JsonReader reader = null;
        try {
            reader = new JsonReader(new FileReader("src/main/resources/data.json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        MessageDto [] messages = gson.fromJson(reader, MessageDto[].class);
        messagesCache = new ArrayList<>(Arrays.asList(messages));
    }
  public  void updateFile(){
      try {
          Writer writer = new FileWriter("src/main/resources/data.json");
          new Gson().toJson(messagesCache, writer);
          writer.close();
      } catch (IOException e) {
          throw  new RuntimeException("Error" +e.getMessage());
      }
  }

  public List<MessageDto> getAllMessages () {
      return messagesCache;
    }

    public void addMesage(MessageDto message) {
        messagesCache.add(message);
        updateFile();
    }

    public void editMessage(MessageUpdateRequestDto message) {
    for ( MessageDto item :messagesCache){
        if(item.getId().equals(message.getId())){
            item.setText(message.getText());
            item.setEditedAt(message.getEditedAt());
            updateFile();
            return;
        }
    }
        System.out.println(messagesCache);
    }
    public void deleteMessage(UUID id){
      messagesCache.removeIf(item->item.id.equals(id));
      updateFile();
    }
}

package com.bamboo.Message;

import lombok.Data;

import java.util.UUID;
@Data
public class MessageUpdateRequestDto {
    UUID id;
    String text;;
    String editedAt;
}

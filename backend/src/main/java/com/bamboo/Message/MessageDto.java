package com.bamboo.Message;

import lombok.Data;

import java.util.Objects;
import java.util.UUID;

@Data
public class MessageDto {
    UUID id;
    UUID userId;
    String avatar;
    String user;
    String text;
    String createdAt;
    String editedAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageDto)) return false;
        MessageDto that = (MessageDto) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

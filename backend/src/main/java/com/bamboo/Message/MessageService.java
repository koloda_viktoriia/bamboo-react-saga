package com.bamboo.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class MessageService {
    @Autowired
    MessageRepository messageRepository;

    public List<MessageDto> getMessages() {

        return messageRepository.getAllMessages();

    }

    public void addMessage(MessageDto message) {
        message.setCreatedAt(ZonedDateTime.now(ZoneOffset.UTC).toString());

         messageRepository.addMesage(message);
    }

    public void updateMessage(MessageUpdateRequestDto message) {
        message.setEditedAt(ZonedDateTime.now(ZoneOffset.UTC).toString());
        messageRepository.editMessage(message);
    }

    public void deleteMessage(UUID id) {
        messageRepository.deleteMessage(id);
    }
}

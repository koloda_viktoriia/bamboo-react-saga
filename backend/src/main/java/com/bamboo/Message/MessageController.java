package com.bamboo.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", allowedHeaders = "*" )
@RestController
@RequestMapping("/messages")
public class MessageController {
    @Autowired
   private MessageService messageService;
 @GetMapping
   public List<MessageDto> getMessages(){
     List<MessageDto> mes = messageService.getMessages();
      return mes;
 }
 @PostMapping
//    @RequestMapping( method = RequestMethod.POST)
    public void addMessage(@RequestBody MessageDto message){


      messageService.addMessage(message);
 }
 @PutMapping
    public void updateMessage(@RequestBody MessageUpdateRequestDto message){
     messageService.updateMessage(message);
 }
 @DeleteMapping("/{id}")
    public void deleteMessage (@PathVariable UUID id){
     messageService.deleteMessage(id);
 }
}

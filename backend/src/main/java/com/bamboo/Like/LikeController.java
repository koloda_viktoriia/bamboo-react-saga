package com.bamboo.Like;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*" )
@RestController
@RequestMapping("/likes")
public class LikeController {
    @Autowired
    LikeService likeService;
    @GetMapping
     public List<LikeDto> getLikes (){
        return likeService.getAllLikes();
    }
    @PostMapping
    public void setLike(@RequestBody LikeDto like){
        System.out.println(like);
        likeService.setLike(like);
    }
    @PutMapping
    public void deleteLike(@RequestBody LikeDto like){
        likeService.deleteLike(like);
    }
}

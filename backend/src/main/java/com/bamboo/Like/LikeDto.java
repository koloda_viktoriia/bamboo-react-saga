package com.bamboo.Like;

import lombok.Data;

import java.util.Objects;
import java.util.UUID;

@Data
public class LikeDto {
    UUID id;
    UUID userId;

    @Override
    public String toString() {
        return "LikeDto{" +
                "id=" + id +
                ", userId=" + userId +
                '}';
    }

}

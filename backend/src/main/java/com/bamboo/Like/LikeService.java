package com.bamboo.Like;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class LikeService {
    @Autowired
    LikeRepository likeRepository;
    public List<LikeDto> getAllLikes() {
        return likeRepository.getAllLikes();
    }

    public void setLike(LikeDto like) {
        likeRepository.setLike(like);
    }

    public void deleteLike(LikeDto like) {
        likeRepository.deleteLike(like);
    }
}

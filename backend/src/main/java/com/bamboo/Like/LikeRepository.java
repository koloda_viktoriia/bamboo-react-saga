package com.bamboo.Like;

import com.bamboo.Message.MessageDto;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class LikeRepository {
    private List<LikeDto> likesCache = new ArrayList<LikeDto>();
    public  List<LikeDto> getAllLikes() {
       return  likesCache;
    }
    LikeRepository(){
        Gson gson = new Gson();
        JsonReader reader = null;
        try {
            reader = new JsonReader(new FileReader("src/main/resources/like.json"));
        } catch (FileNotFoundException e) {
            throw  new RuntimeException(e.getMessage());
        }
        LikeDto[] likes = gson.fromJson(reader, LikeDto[].class);
        likesCache = new ArrayList<>(Arrays.asList(likes));
    }
    public void updateFile(){
        try {
            Writer writer = new FileWriter("src/main/resources/like.json");
            new Gson().toJson(likesCache, writer);
           writer.close();
        } catch (IOException e) {
            throw  new RuntimeException("Error" +e.getMessage());
        }
    }

    public void setLike(LikeDto like) {
        System.out.println(like);
        likesCache.add(like);
        updateFile();
    }
    public void deleteLike(LikeDto like){
        likesCache.remove(like);
        updateFile();
    }
}

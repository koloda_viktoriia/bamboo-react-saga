package com.bamboo.User;

import lombok.Data;

import java.util.UUID;

@Data
public class UserDto {
    UUID userId;
    String user;
    String password;
    String avatar;
    String role;

  }

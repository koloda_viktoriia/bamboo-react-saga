package com.bamboo.User;

import com.bamboo.Message.MessageDto;
import com.bamboo.Message.MessageUpdateRequestDto;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.*;

@Repository
public class UserRepository {
    List<UserDto> usersCache;
    UserRepository(){
        Gson gson = new Gson();
        JsonReader reader = null;
        try {
            reader = new JsonReader(new FileReader("src/main/resources/user.json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        UserDto[] users = gson.fromJson(reader, UserDto[].class);
        usersCache = new ArrayList<>(Arrays.asList(users));
    }
    public void updateFile(){
        try {
            Writer writer = new FileWriter("src/main/resources/user.json");
            new Gson().toJson(usersCache, writer);
            writer.close();
        } catch (IOException e) {
            throw  new RuntimeException("Error" +e.getMessage());
        }
    }
    public List<UserDto> getAllUsers() {
        return  usersCache;
    }

    public UserDto authenticate(LoginDto user) {
        for (UserDto item : usersCache){
            if(item.getUser().equals( user.getUser()) && item.getPassword().equals(user.getPassword())){
                return item;
            }
        }
        return null;
    }

    public void addUser(UserDto user) {
        usersCache.add(user);
        updateFile();
    }
    public void editUser(UserDto user) {
        for ( UserDto item :usersCache){
            if(item.getUserId().equals(user.getUserId())){
                item.setUser(user.getUser());
                item.setRole(user.getRole());
                item.setPassword(user.getPassword());
                item.setAvatar(user.getAvatar());
                updateFile();
                return;
            }
        }
    }

    public void deleteUser(UUID id){
        usersCache.removeIf(item->item.getUserId().equals(id));
        updateFile();
    }

    public Optional<UserDto> findByUser(String user) {
        for(UserDto item: usersCache){
            if(item.getUser().equals(user))
                return Optional.of(item);
        }
        return null;

    }
}

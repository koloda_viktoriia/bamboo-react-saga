package com.bamboo.User;

import lombok.Data;

@Data
public class LoginDto {
    String user;
    String password;
}

package com.bamboo.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*" )
@RestController
@RequestMapping("/login")
public class AuthController {
    @Autowired
    UserService userService;
    @PostMapping
    public  UserDto login (@RequestBody LoginDto user){
     return userService.login(user);
    }
}

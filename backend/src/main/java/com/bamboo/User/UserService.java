package com.bamboo.User;

import com.bamboo.Message.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService   {
    @Autowired
    UserRepository userRepository;
    public List<UserDto> getUsers() {
        return  userRepository.getAllUsers();
    }

    public UserDto login(LoginDto user) {
        return userRepository.authenticate(user);
    }

    public void addUser( UserDto user) {
        userRepository.addUser(user);
    }


    public void deleteUser(UUID id) {
        userRepository.deleteUser(id);
    }

    public void updateUser(UserDto user) {
        userRepository.editUser(user);
    }

}

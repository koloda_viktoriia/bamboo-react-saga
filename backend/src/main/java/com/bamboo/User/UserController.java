package com.bamboo.User;


import com.bamboo.Message.MessageDto;
import com.bamboo.Message.MessageUpdateRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", allowedHeaders = "*" )
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

   @GetMapping
    public List<UserDto> getUsers(){
       return userService.getUsers();
   }
    @PostMapping
//    @RequestMapping( method = RequestMethod.POST)
    public void addUser(@RequestBody UserDto user){
        userService.addUser(user);
    }

    public UserService getUserService() {
        return userService;
    }

    @PutMapping
    public void updateUser(@RequestBody UserDto user){
       userService.updateUser(user);
    }
    @DeleteMapping("/{id}")
    public void deleteMessage (@PathVariable UUID id){
        userService.deleteUser(id);
    }

}
